# TubesSister-chat-menggunakan-web-socket
# Daftar kelompok
1. Fatahillah Karomy (1301164346)
2. Rista Aryantiwi (1301162387)
3. Muhammad Hafiz Zamrudin (1301164259)
4. M. Raihan Rafiiful Allaam (1301164162)

# Penjelasan Aplikasi
Chat App menggunakan Websocket merupakan sebuah layanan chat berbasis website yang menggunakan fitur websocket. Dimana user yang menggunakan Chat App ini dapat saling bertukar pesan. Contohnya user A, B dan C join kedalam Chat App, kemudian user A mengirimkan sebuah pesan ke ruang obrolan, di lain tempat user B dan user C yang sebelumnya telah join dapat membaca pesan yang telah dikirimkan oleh user A, kemudian user B dan C dapat membalas pesan tersebut. Di waktu yang bersamaan user D join kedalam chat, namun tidak dapat melihat history chat sebelum user D join kedalam ruang obrolan.

# Fitur
1. Login
2. Timestamp
3. History Chat
4. Clear History

# Requirement
1. Python 3.7 or Newer
2. Web Browser
3. Websockets Package

# Wajib dijalankan

Install Package websockets with pip3

```
pip3 install websockets
```
